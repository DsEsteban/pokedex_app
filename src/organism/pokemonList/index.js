import React, { useState, useEffect, useCallback, useRef } from 'react'; 
import PokemonCard from '../../molecules/pokemonCard'; 
import { useFetchPokemonList } from '../../hooks';
import { pokemonListConstants } from '../../utils/const';

const PokemonList = () => {

    const [pagination, setPagination] = useState(0);
    const [pokemonListArr, setPokemonListArr] = useState([]);
    const listPokemons = useFetchPokemonList(pagination, pokemonListConstants.limitPokemonsByRequest);

    let bottomBoundaryRef = useRef(null);
    const scrollObserver = useCallback(
        node => {
            new IntersectionObserver(entries => {
                entries.forEach(en => {
                    if (en.intersectionRatio > 0) {
                        setPagination(pagination => pagination + 10)
                    }
                });
            }).observe(node);
        },
        []
    );

    useEffect(() => {
        if (bottomBoundaryRef.current) {
            scrollObserver(bottomBoundaryRef.current);
        }
    }, [scrollObserver, bottomBoundaryRef]);

    useEffect(() => {
        setPokemonListArr(pokemonListArr => [...pokemonListArr, ...listPokemons]);
    },[listPokemons]);

    return (
        <>
            <ul style={{ display: 'flex', listStyleType: 'none', flexWrap:'wrap' }}>
                    {
                        pokemonListArr.map(
                            (pokemonData, index) => 
                            <li key={`${pokemonData.name}_${index}`}>
                                <PokemonCard 
                                    name={pokemonData.name}
                                    url={pokemonData.url}
                                />
                            </li>
                        )
                    }
            </ul>
            <div id='page-bottom-boundary' ref={bottomBoundaryRef} style={{height:'2px'}}>
                <p>Loading more...</p>
            </div>
            
        </>
    )
}

export default PokemonList;