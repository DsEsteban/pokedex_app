import React from 'react'; 
import { useGetPokemonFullDataWithId } from '../../hooks';
import { useParams } from "react-router-dom";


const PokemonDetail = () => {
    let { id } = useParams();
    const pokemonData = useGetPokemonFullDataWithId(id);
    return (
        <div className={'wrapper_pokemon_detail'}>
            {
                pokemonData && 
                <div >
                    <h2>{pokemonData.name}</h2>
                    <img src={pokemonData.sprites.front_default} alt={pokemonData.name} />
                    <div>General Info:</div>
                    <div>
                        <ul aria-label="Details:">
                            <li>weight: {pokemonData.weight}</li>
                            <li>
                                <label>Types</label>
                                <ul aria-label="types:">
                                    {
                                        pokemonData.types.map(
                                            ({ type }) => <li key={type.name}>{type.name}</li>
                                            )
                                        }
                                    
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            }
        </div>
    )
}

export default PokemonDetail;