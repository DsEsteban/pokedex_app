import React from "react"; 

const Template = ({ header, main, footer }) => {

    return (
        <>
            {header}
            <main>{main}</main>
            {footer}
        </>
    );
}

export default Template;