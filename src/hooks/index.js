import { useState, useEffect } from 'react'; 
import { POKEMON_API_URL } from '../utils/urls';


export const useFetchPokemonList = (offset, limit) => {
    const [data, setData] = useState([]);

    useEffect(() => {
        const cachedData = localStorage.getItem(POKEMON_API_URL)
        if (cachedData) {
            setData(JSON.parse(cachedData))
        } else {
            fetch(`${POKEMON_API_URL}/pokemon/?limit=${limit}&offset=${offset}`)
                .then(response => response.json())
                .then(({results}) => { console.log('json=>', offset, results); setData(results)})
        }

    }, [offset, limit]);

    return data;
}

export const useGetPokemonMinData = ( POKEMON_URL ) => {
    const [data, setData] = useState({imageUrl:'', id: ''});
    
    useEffect(() => {
         
        fetch(`${POKEMON_URL}`)
            .then(response => response.json())
            .then(({ sprites, id }) => setData({imageUrl: sprites.front_default, id}) ) 

    },[])
    
    return data;
}

export const useGetPokemonFullData = ( POKEMON_URL ) => {
    const [data, setData] = useState(null);
    
    useEffect(() => {
         
        fetch(`${POKEMON_URL}`)
            .then(response => response.json())
            .then((data) => setData(data) ) 

    },[])
    
    return data;
}

export const useGetPokemonFullDataWithId = ( id ) => {
    const [data, setData] = useState(null);
    
    useEffect(() => {
         
        fetch(`${POKEMON_API_URL}/pokemon/${id}`)
            .then(response => response.json())
            .then((data) => setData(data) ) 

    },[])
    
    return data;
}