export const getPokemonPhoto = ( POKEMON_URL ) => {

    const response = fetch(`${POKEMON_URL}`)
        .then(response => response.json())
        .then(({results}) => results.sprites.front_default )
    
    return response;
}
