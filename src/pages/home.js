import HomeTemplate from '../templates'
import PokemonList from '../organism/pokemonList'

const HomePage = () => {
    return (
        <HomeTemplate 
            header={<></>}
            main={
                <PokemonList />
            }
            footer={<></>}
        />
    )
}

export default HomePage;