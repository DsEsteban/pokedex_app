import Template from '../templates'
import PokemonDetail from '../organism/pokemonDetailCard'

const DetailPage = () => {
    return (
        <Template 
            header={<></>}
            main={
                <PokemonDetail />
            }
            footer={<></>}
        />
    )
}

export default DetailPage;