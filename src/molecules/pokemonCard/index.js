import React from 'react'; 
import { useGetPokemonMinData } from '../../hooks'
import {
    Link
  } from "react-router-dom"

const PokemonCard = ({ name, url }) => {
    const { imageUrl, id } = useGetPokemonMinData(url);

    return (
        <Link to={`/pokemon/${id}`}>
            <div style={{
                height: 250,
                width: 250
            }}>
                <h3>{name}</h3>
                    {imageUrl && <img src={imageUrl} alt={`${name}`} /> }
                </div>
        </Link>
    )
}

export default PokemonCard;