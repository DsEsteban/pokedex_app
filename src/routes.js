import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import HomePage from './pages/home';
import DetailPage from './pages/detail';

const RoutesPage = () => {
    return (
        <Router>
            <Switch>
                <Route path="/pokemon/:id">
                    <DetailPage />
                </Route>
                <Route path="/">
                    <HomePage />
                </Route>
            </Switch>
        </Router>
    );
} 

export default RoutesPage;